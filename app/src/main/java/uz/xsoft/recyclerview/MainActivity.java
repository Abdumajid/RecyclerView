package uz.xsoft.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AlphaAnimation;

import com.thedeanda.lorem.LoremIpsum;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{
    private RecyclerView recyclerView;
    private ArrayList<MyModel> list;
    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        loadList();
        adapter = new MyAdapter(list, this);
        recyclerView.setAdapter(adapter);

    }

    private void loadList() {
        list = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            list.add(new MyModel(
                    LoremIpsum.getInstance().getFirstName(),
                    LoremIpsum.getInstance().getEmail(),
                    "https://picsum.photos/200/300?image=" + i));
        }
    }

    private void init() {
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAnimation(new AlphaAnimation(0.2f, 0.2f));
    }

}
