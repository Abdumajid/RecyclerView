package uz.xsoft.recyclerview;

public class MyModel {
    private String headerText;
    private String subText;
    private String imageURL;

    public MyModel(String headerText, String subText, String imageURL) {

        this.headerText = headerText;
        this.subText = subText;
        this.imageURL = imageURL;
    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public String getSubText() {
        return subText;
    }

    public void setSubText(String subText) {
        this.subText = subText;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
