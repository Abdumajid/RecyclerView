package uz.xsoft.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MyViewHolder extends RecyclerView.ViewHolder {
    private TextView headerText;
    private TextView subText;
    private ImageView image;

    public MyViewHolder(View itemView) {
        super(itemView);
        headerText = itemView.findViewById(R.id.header);
        subText = itemView.findViewById(R.id.sub);
        image = itemView.findViewById(R.id.imageview);
    }

    void bind(MyModel data) {
        headerText.setText(data.getHeaderText());
        subText.setText(data.getSubText());
        Picasso.get().load(data.getImageURL()).resize(100,100).into(image);
    }
}
